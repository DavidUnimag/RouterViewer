'use strict'

const mongoose = require('mongoose') //ODM de mongodb 
const Schema = mongoose.Schema;


const RutaSchema = Schema({
    name: String,
    barrios:[{
        name: {type: String, lowercase: true},
        Lat: Number,
        Lon: Number
    }],
    URL: String
})

module.exports = mongoose.model('Ruta', RutaSchema);