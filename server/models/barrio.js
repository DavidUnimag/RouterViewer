'use strict'

const mongoose = require('mongoose') //ODM de mongodb 
const Schema = mongoose.Schema;

const BarrioSchema = new Schema({
    name: String,
    Lat: Number,
    Lon: Number
})

module.exports = mongoose.model('Barrio', BarrioSchema);