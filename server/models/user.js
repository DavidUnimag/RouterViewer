'use strict'

const mongoose = require('mongoose') //ODM de mongodb 
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs'); //Permite encriptar datos, en este caso la contraseña. 

const UserSchema = Schema({
    name: String,
    email: {type: String, unique: true, lowercase: true},
    password: {type: String, select: false}
})

/**La función 'pre' permite llamar una función antes de que se ejecute X función (de mongoose).
 * En este caso antes de que se ejecute la función 'save' en cualquier lugar de algún
 * controlador de usuario. 
 */
UserSchema.pre('save', function(next){
    let user = this
    if(!user.isModified('password')) return next()
    /**Función de encriptado */
    bcrypt.genSalt(10, (err, salt) =>{
        if(err) return next(err)
        bcrypt.hash(user.password, salt, null, (err, hash) =>{
            if(err) return next(err)
            user.password = hash;
            next();
        })
    })
}) 

/** Desencriptar contraseña */
UserSchema.methods.comparePassword = function (loginPassword, callBack) {
    bcrypt.compare(loginPassword, this.password, (err, isMatch) => {
      callBack(err, isMatch)
    });
  }

module.exports = mongoose.model('User', UserSchema);