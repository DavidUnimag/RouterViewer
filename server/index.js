'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const api  = require ('./routes');
const mongoose = require('mongoose');
const cors = require('cors');


const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors()); 
app.use('/api', api);

app.get('/sal', (req, res) =>{
    let saludo = "Hola";
    res.status(200).send({message: 'Hola'})
})


mongoose.connect(config.db,{useCreateIndex:true,useNewUrlParser: true} ,(err, res) =>{
    if(err){
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    console.log("Conexión a la base de datos establecida");
    app.listen(config.port, ()=>{
        console.log(`Api-rest corriendo en el puerto ${config.port}`);
    })
})
