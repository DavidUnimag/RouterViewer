'use strict'


const Barrio = require('../models/barrio')


function saveBarrio(req, res){
    const barrio = new Barrio({
        name: req.body.name,
        Lat: req.body.Lat, 
        Lon: req.body.Lon
});

    barrio.save((err) =>{
        if(err) res.status(500).send({message: `Error al crear el barrio: ${err}`});
        return res.status(200).send(barrio);
    })
}

function getBarrios(req, res){
    Barrio.find({}, (err, barrio) =>{
        if(err) return res.status(500).send({message: err});
        if(!barrio) return res.status(404).send({message: 'No hay barrios'})
        return res.status(200).send({barrio: barrio});
    })
}

module.exports = {
    saveBarrio,
    getBarrios
}