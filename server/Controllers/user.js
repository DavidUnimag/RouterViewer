
'use strict'

const moongose = require('mongoose')
const User = require('../models/user')


function signUp(req, res){
    const user = new User({
        email: req.body.email,
        name: req.body.name,
        password: req.body.password
    })
    user.save((err) =>{
        if(err) res.status(500).send({message: `Error al crear el usuario: ${err}`})
        return res.status(200).send(user);
    })
}

/**Debido a que en la especificaciones del conjunto en la carpeta 'models' decimos que
 * la contraseña es 'select: false', cuando hacemos una petición a la base de datos, NO nos 
 * devolverá la contraseña. En este caso especifico (Login) necesitamos saber la contraseña
 * para poder desencriptarla y compararla con la contraseña brindada por el usuario, esto se soluciona 
 * poniendo el '.select('email password') al final de la función 'User.findone', cuando la función 
 * haga la petición nos devolverá el email y contraseña alojadas en la base de datos. 
 */

function singIn(req, res){
    User.findOne({email: req.body.email}, (err, user) =>{
        if(err) return res.status(500).send({message: err});
        if(!user) return res.status(404).send({message: 'Nombre de usuario inexistente'});
        
        console.log("user en controlador: "+user);
        return user.comparePassword(req.body.password, (err, isMatch) =>{
            if (err) return res.status(500).send({ msg: `Error al ingresar: ${err}` });
            if (!isMatch) return res.status(404).send({ msg: `Error de contraseña: ${req.body.email}` });
            return res.status(200).send({message: 'Te has logeado correctamente'})
        });
    }).select('email password');
}



module.exports = {
    signUp,
    singIn
}