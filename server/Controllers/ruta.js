'use strict'

const Ruta = require('../models/ruta');


function findRoute(req, res){
    const origen = req.body.origen;
    const destino = req.body.destino;
    Ruta.findOne({$and:[{'barrios.name': origen},{'barrios.name': destino}]}, (err, ruta)=>{
        if(err) return res.status(500).send({message: err})
        if(!ruta) return res.status(404).send({message: 'No existe la ruta'}) //mod
        return res.status(200).send({ruta: ruta});
    })
}

function saveRuta(req, res){
    const ruta = new Ruta({
        name: req.body.name,
        barrios: [
            {
                name: 'Centro',
                lat: 10022,
                lon: 12234
            },
            {
                name: 'María Eugenia',
                lat: 21222,
                lon: 23332
            },
            {
                name: 'La 30',
                lat: 21222,
                lon: 23332
            },
            {
                name: 'Bonda',
                lat: 21222,
                lon: 23332
            },
        ],
        URL: req.body.url
    })

    ruta.save((err) => {
        if(err) res.status(500).send({message: 'Error al crear la ruta: ' + err});
        res.status(200).send('Ruta creada exitosamente: ' +ruta);
    })
    
}

function getRuras(req, res){
    Ruta.find({}, (err, rutas) =>{
        res.status(200).send({rutas});
    })
}

module.exports = {
    saveRuta,
    findRoute,
    getRuras
}