'use strict'

const express = require('express')
const api = express.Router()

//llamado a los controladores
const userController = require('../Controllers/user')
const rutaController = require('../Controllers/ruta')
const barrioContoller = require('../Controllers/barrio')

//Peticiones http 
api.post('/singup', userController.signUp);
api.post('/singin', userController.singIn);

api.post('/savebarrio', barrioContoller.saveBarrio);
api.get('/getbarrios', barrioContoller.getBarrios);

api.post('/findruta', rutaController.findRoute);
api.post('/saveruta', rutaController.saveRuta);
api.get('/getrutas', rutaController.getRuras);

api.get('/hola', (req, res) =>{
    res.status(200).send({message: 'Tienes acceso'})
})


module.exports = api

