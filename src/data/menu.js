export default [ 
    {
        title: 'Home',
        url: '/'
    },
    {
        title: 'About us',
        url: '/about'
    },
    {
        title: 'Find You Route',
        url: '/find'
    },
    {
        title: 'Login',
        url: 'login'
    }
];