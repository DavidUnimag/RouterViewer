import React, { Component } from 'react';
import './css/Content.css';

class Content extends Component {

  constructor(){
    super();
    this.state = {
      count: 0
    };
    
    //Con esto permitimos que el método obtenga todo el objeto this de la clase (tenga acceso a los state, props e.t.c)
    this.handleCountClick = this.handleCountClick.bind(this);
    this.handleResultClick = this.handleResultClick.bind(this);
    this.handleInputChanged = this.handleInputChanged.bind(this);
  }

  // Este metodo sirve para comprobar si el componente (en este caso el componente Content) ha cargado. 
  componentDidMount(){
    this.setState({
      count: 1,
      number1: 0,
      number2: 0,
      result: 0
    });
  }

  handleCountClick(event){
    if(event.target.id === 'add'){
      this.setState({
        count: this.state.count + 1
      });
    }else if(event.target.id === 'substract' && this.state.count > 0){
      this.setState({
        count: this.state.count - 1
      });
    }else if(event.target.id === 'reset'){
      this.setState({
        count: 0
      });
    }
  }

  handleResultClick(event){
    this.setState({
      result: this.state.number1 + this.state.number2
    });
  }

  handleInputChanged(event){
    if(event.target.id === 'number1'){
      this.setState({
        number1: Number(event.target.value)
      });
    }else if(event.target.id === 'number2'){
      this.setState({
        number2: Number(event.target.value)
      });
    }
  }

  render() {
    return (
      <div className="Content">
        <h2>Counter: {this.state.count}</h2>
        <p>
          <button id="add" onClick={this.handleCountClick}>Add</button>
          <button id="substract" onClick={this.handleCountClick}>Substract</button>
          <button id="reset" onClick={this.handleCountClick}>Reset</button>
        </p>

        <p>
          <input id="number1" type="number" value={this.state.number1} onChange={this.handleInputChanged}/>
          +
          <input id="number2" type="number" value={this.state.number2} onChange={this.handleInputChanged}/>
          <button id="result" onClick={this.handleResultClick}>=</button>
          <h2>Result: {this.state.result}</h2>
        </p>
      </div>
    );
  }
}

export default Content;
