// Dependencias:
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Assets:
import './css/Footer.css';

class Footer extends Component {
  static propTypes = {
    copyright: PropTypes.string,
  };
  render() {
    const {copyright = '&copy; React App 2018'} = this.props;
    return (
    <footer>
      <p>{copyright}</p>
    </footer>
    );
  }
}

export default Footer;
