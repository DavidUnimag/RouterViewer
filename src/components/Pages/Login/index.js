// Dependencias:
import React, { Component } from 'react';
import './login.css'

class Login extends Component{

    constructor(){
        super()
        this.state = {
            isLogin: true
          };
        
        this.changedStateLogin = this.changedStateLogin.bind(this);
        this.renderLogin = this.renderLogin.bind(this);
        this.renderSingUp = this.renderSingUp.bind(this);
        this.handleWathRender = this.handleWathRender.bind(this);
    }

    changedStateLogin(){
        this.setState({isLogin: !this.state.isLogin})
    }

    handleWathRender(){
        switch(this.state.isLogin){
            case true: return(this.renderLogin());
            case false: return(this.renderSingUp());
            default: return(<p>Ha ocurrido un error</p>);
        }
    }

    renderLogin(){
        return(
            <div className="logincard">
                <div className="card">
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Email"/>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" id="exampleFormControlInput2" placeholder="password"/>
                            </div>
                        </form>
                        <p className="btn btn-primary">Login</p>
                        <p className="singuptext">You dont have an account? please, Sing Up <p className="changedWithoutLink" onClick={this.changedStateLogin}>here</p></p>
                    </div>
                </div>
            </div>
        )
    }

    renderSingUp(){
        return(
            <div className="logincard">
                <div className="card">
                    <div className="card-body">
                        <form>
                            <div className="form-group">
                                <input className="form-control" id="exampleFormControlInput2" placeholder="User name"/>
                            </div>
                            <div className="form-group">
                                <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Email"/>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" id="exampleFormControlInput2" placeholder="password"/>
                            </div>
                        </form>
                        <p className="btn btn-primary">Sing Up</p>
                        <p className="singuptext">If you already have an account, please click <p onClick={this.changedStateLogin} className="changedWithoutLink">here</p></p>
                    </div>
                </div>
            </div>
        )
    }

    render(){  
        return(
            <div className="Login">
                {this.handleWathRender()}
            </div>
        );
    }
}

export default Login;