// Dependencias:
import React, { Component } from 'react';
import axios from 'axios'
import { compose, withProps } from "recompose"
import { withScriptjs, withGoogleMap, GoogleMap, KmlLayer} from "react-google-maps"
import './mapa.css'


const MyMapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBRg9vHcSjoHXGkbJbMKK_PXDA5EwCnEFE&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `600px` }} />,
        mapElement: <div style={{ height: `100%`, width:'76%', float: "right"}} />,
    }),
    withScriptjs,
    withGoogleMap
  )(props =>
    <GoogleMap
      defaultZoom={15}
      defaultCenter={{lat: 11.2407900, lng: -74.1990400}}
    >
      <KmlLayer
        url={props.url}
        options={{ preserveViewport: true }}
      />
    </GoogleMap>
  );
  

class MapContainer extends Component{

    constructor(props){
        super(props);
        this.state = {
            url: null,
            origenInput: '',
            destinoInput:''
        }
        this.handleFindRoute = this.handleFindRoute.bind(this);
        this.updateOrigenValue = this.updateOrigenValue.bind(this);
        this.updateDestinoValue = this.updateDestinoValue.bind(this);
    }

    updateOrigenValue(evt){
        this.setState({
            origenInput: evt.target.value
        });
    }

    updateDestinoValue(evt){
        this.setState({
            destinoInput: evt.target.value
        });
    }

   async handleFindRoute(){
      
        const proxy = 'http://localhost:5000/api/findruta';
        try{
            const response = await axios.post(proxy,{
                origen: this.state.origenInput,
                destino: this.state.destinoInput
            });
            console.log(response.data.ruta.URL);
            this.setState({url: response.data.ruta.URL})
        }catch(error){
            console.error(error);
        }   
    }

    render(){
        return(
            <div className="MapComponent">
                <div className="findMapInput">
                    <div className="imageFind">
                        <img src={require('./stamta.jpg')} alt="Find header" height="100px" width="100%"/>
                    </div>
                    <div className="findInput">
                        <div className="input-group mb-3 probe">
                            <div className="input-group-prepend">
                                <span className="input-group-text pretendInput" id="basic-addon1">Inicio</span>
                            </div>
                            <input value={this.state.origenInput} onChange={this.updateOrigenValue} type="text" className="form-control inputTextFind" placeholder="Selecciona origen"></input>
                        </div>
                        <div className="input-group mb-3 probe">
                            <div className="input-group-prepend">
                                <span className="input-group-text pretendInput" id="basic-addon1">Fin</span>
                            </div>
                            <input value={this.state.destinoInput} onChange={this.updateDestinoValue} type="text" className="form-control inputTextFind" placeholder="Selecciona destino"></input>
                        </div>
                        <button type="button" className="btn btn-danger" onClick={this.handleFindRoute}>Buscar</button>
                    </div>
                    <div className="recientesTitle">
                        <p>Recientes</p>
                    </div>
                </div>
                <div className="map">
                    <MyMapComponent url={this.state.url}/>
                </div>
            </div>
        );
    }
}

export default MapContainer;