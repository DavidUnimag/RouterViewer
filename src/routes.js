// Dependencias:
import React from 'react';
import {Route, Switch} from 'react-router-dom';

// Componentes:
import App from './components/App';
import About from './components/Pages/About';
import Mapa from './components/Pages/Mapa';
import Home from './components/Pages/Home';
import Page404 from './components/Pages/Page404';
import Login from './components/Pages/Login';

const AppRoutes = () =>
    <App>
        <Switch>
            <Route exact path="/about" component={About}/>
            <Route exact path="/find" component={Mapa}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/" component={Home}/>
            <Route exact component={Page404}/>
        </Switch>
    </App>


export default AppRoutes;